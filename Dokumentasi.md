# Kubernetes Service Mesh For Microservices Application

## Spesifikasi Cluster 
1. Master Node
    - OS = CentOS 7
    - VCPU = 2 Core
    - RAM =  2 GB

2.  Worker 1
    - OS = CentOS 7
    - VCPU = 2 Core
    - RAM =  2 GB

3. Worker 2
    - OS = CentOS 7
    - VCPU = 2 Core
    - RAM =  2 GB

- [Langkah installasi Kubernetes di CentOS 7<br>](https://github.com/adaptivenetlab/kubernetes/blob/main/Intallation/centos7.md)

# Set MetalLB LoadBalancer 

```bash
kubectl apply -f metallb/namespace.yaml
kubectl apply -f metallb/metallb.yaml
```
## On first install only
```bash
kubectl create secret generic -n metallb-system memberlist --from-literal=secretkey="$(openssl rand -base64 128)"
```

## Layer 2 Configuration
change adresses with usable ip from your network ( you can check from ip network of your master node )
```bash
kubectl apply -f metallb/cm-l2.yaml
```

## Verify that metallb has successfully deployed
```bash
kubectl get all -n metallb-system
```
you can see there are 4 pods running, 1 controller and 3 speakers.

# Install Istio

## Download Istio file
```bash
curl -L https://istio.io/downloadIstio | sh -
cd istio-1.9.2
export PATH=$PWD/bin:$PATH
```
## Install the Istio Operator
```bash
istioctl operator init  
kubectl get all -n istio-operator
```
## Apply istio
```bash
kubectl create ns istio-system
kubectl apply -f - <<EOF
apiVersion: install.istio.io/v1alpha1
kind: IstioOperator
metadata:
  namespace: istio-system
  name: example-istiocontrolplane
spec:
  profile: demo
EOF
```
## Check if Istio has sucessfully installed
```bash
kubectl get all -n istio-system
```
Lihat service istio-ingressgateway, kita harus mengubah tipe servicenya menjadi LoadBalancer supaya bisa diakses dan mendapatkan external-ip
```bash
kubectl edit svc istio-ingressgateway -n istio-system
```
ganti type service dari ClusterIP menjadi LoadBalancer, lalu cek IP eksternal yang didapatkan
```bash
kubectl get svc istio-ingressgateway -n istio-system
```

# Apply Microservices Application
## Bookinfo Application
Create namespace bookinfo
```bash
kubectl create ns bookinfo
```

Create istio injection label
```bash
kubectl get ns bookinfo --show-labels
kubectl label ns bookinfo istio-injection=enabled
```

# Apply microservice ( Deployment + Service )
```bash
kubectl apply -f bookinfo-app/bookinfo.yaml -n bookinfo
```
## Apply Gateway dan Virtual Service
```bash
kubectl apply -f bookinfo-app/bookinfo-gateway.yaml -n bookinfo
```
## Pastikan Seluruh Komponen Terinstall
```bash
kubectl get all -n bookinfo
kubectl get gateway -n bookinfo
kubectl get virtualservice -n bookinfo
```
Access : 
```bash 
http://<ip-external-istio-ingressgateway>/productpage
```

# Apply Prometheus 

```bash 
kubectl apply -f addons/prometheus.yaml -n istio-system
kubectl get pod -n istio-system
```
# Apply Grafana Dashboard
```bash
kubectl apply -f addons/grafana -n istio-system
```

lihat service grafana
```bash
kubectl get svc grafana -n istio-system
```

kita harus mengganti service grafana agar bisa diakses melalui service loadbalancer

```bash
kubectl edit svc grafana -n istio-system
```

ubah type service ClusterIP menjadi LoadBalancer, lalu cek lagi IP external yang diperoleh

```bash
kubectl get svc grafana -n istio-system
```

akses dashboard grafana melalui IP external yang diperoleh
```bash
http://<ip-external-service-grafana>:3000
```

Dashboard Grafana untuk monitoring service mesh melalui istio sudah siap digunakan

# Apply Kiali Dashboard 
```bash
kubectl apply -f addons/kiali-crd.yaml
kubectl apply -f addons/kiali -n istio-system
```

lihat service kiali
```bash
kubectl get svc kiali -n istio-system
```

kita harus mengganti service kiali agar bisa diakses melalui service loadbalancer

```bash
kubectl edit svc kiali -n istio-system
```

ubah type service ClusterIP menjadi LoadBalancer, lalu cek lagi IP external yang diperoleh

```bash
kubectl get svc kiali -n istio-system
```

akses dashboard grafana melalui IP external yang diperoleh
```bash
http://<ip-external-service-kiali>:20001
```

Dashboard Kiali untuk monitoring service mesh melalui istio sudah siap digunakan

## Testing Inject Traffic 
1. Linux Terminal
```bash
watch -n <jeda waktu> curl -o /dev/null -s -w %{http_code} 192.168.0.101/produ
ctpage
```
or 

2. Windows PowerShell
```bash
While ($true) { curl -UseBasicParsing http://192.168.0.101/productpage;Start-Sleep -Seconds <jeda waktu>;} 
```
note: 
- Jeda waktu merupakan jarak waktu antar looping pemberian traffic
- 192.168.0.101 merupakan IP istio ingress gateway 


## Installasi istio dan penggunaanya selesai

source : 
- https://istio.io/latest/docs/setup/install/operator/
- https://istio.io/latest/docs/examples/bookinfo/
- https://metallb.universe.tf/installation/

# Troubleshoot
- Jika metallb tidak assign IP secara otomatis, maka terjadi ketidaksesuaian antara flannel network dan subnet. 

untuk mengatasinya, bisa mengubah flannel network agar sama seperti flannel subnet

### /var/run/flannel/subnet.env

```bash 
FLANNEL_NETWORK=20.30.0.0/16
FLANNEL_SUBNET=20.30.0.1/24
FLANNEL_MTU=1450
FLANNEL_IPMASQ=true
```
pastikan FLANNEL_SUBNET sesuai dengan FLANNEL_NETWORK

jika tidak sesuai maka apply ulang [kubeflannel.yaml](./kube-flannel.yaml) dengan mengganti value network dengan ip network yang sesuai 
```bash
net-conf.json: |
    {
      "Network": "20.30.0.0/16",
      "Backend": {
        "Type": "vxlan"
      }
    }
```